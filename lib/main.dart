import 'package:flutter/material.dart';

void main() => runApp(MaterialApp(
  home: Home(),
));

class Home extends StatefulWidget {

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {

  int number = 0;
  int square=0,cube=0;
  String text="Number is: ";
  String square_text="Square of the number ";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text('Maths tutorials'),
        centerTitle: true,
        backgroundColor: Colors.purple,
        elevation: 0,
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          setState(() {
            number++;
            square=number*number;
            cube=number*number*number;
            square_text;
          });
        },
        child: Icon(Icons.hdr_strong),
        backgroundColor: Colors.green,
      ),
      body:
      new ListView(
          children: <Widget>[
            Padding(
        padding: EdgeInsets.fromLTRB(30, 40, 30, 0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Center(
              child: Image.asset('images/giphy.gif'),

            ),
            Center(
              child: Text(
                'Welcome to maths tutorial basics',
                style: TextStyle(
                  color: Colors.black,
                  fontSize: 18,
                  letterSpacing: 2,
                ),
              ),
            ),
            SizedBox(height: 10),
            Row(

              children: <Widget>[
                SizedBox(
                  width: 30,
                ),
                Icon(
                  Icons.add,
                  color: Colors.green,
                  size: 35,
                ),
                SizedBox(
                  width: 50,
                ),
                Text(
                  '-',
                  style: TextStyle(
                      color: Colors.orange,
                      letterSpacing: 2,
                      fontSize: 35,
                      fontWeight: FontWeight.bold
                  ),
                ),
                SizedBox(
                  width: 50,
                ),
                Text(
                  '/',
                  style: TextStyle(
                      color: Colors.yellow,
                      letterSpacing: 2,
                      fontSize: 35,
                      fontWeight: FontWeight.bold
                  ),
                ),
                SizedBox(
                  width: 50,
                ),
                Icon(
                  Icons.close,
                  color: Colors.deepPurple,
                  size: 35,
                ),

              ],

            ),
            SizedBox(height: 10),
            Text(
              'Number',
              style: TextStyle(
                  color: Colors.blueAccent,
                  fontSize: 25,
                  fontWeight: FontWeight.bold
              ),
            ),
            Text(
              '$number',
              style: TextStyle(
                  color: Colors.pink,
                  fontSize: 28,
                  fontWeight: FontWeight.bold
              ),
            ),
            Divider(
              height: 20,
              color: Colors.grey[800],
            ),
            Text(
              '$square_text',
              style: TextStyle(
                  color: Colors.blueAccent,
                  letterSpacing: 2,
                  fontSize: 25,
                  fontWeight: FontWeight.bold
              ),
            ),
            Text(
              '$square',
              style: TextStyle(
                  color: Colors.pink,
                  letterSpacing: 2,
                  fontSize: 28,
                  fontWeight: FontWeight.bold
              ),
            ),
            Divider(
              height: 20,
              color: Colors.grey[800],
            ),
            Text(
              'Cube of the number',
              style: TextStyle(
                  color: Colors.blueAccent,
                  letterSpacing: 2,
                  fontSize: 25,
                  fontWeight: FontWeight.bold
              ),
            ),
            Text(
              '$cube',
              style: TextStyle(
                  color: Colors.pink,
                  letterSpacing: 2,
                  fontSize: 28,
                  fontWeight: FontWeight.bold
              ),
            ),

            SizedBox(height: 20),
            Text(
              'Click the right corner button \nto check',
              style: TextStyle(
                  color: Colors.grey,
                  letterSpacing: 2,
                  fontSize: 15,
                  fontWeight: FontWeight.bold
              ),

            ),
          ],

        ),
      ),
],
      ),
    );
  }
}